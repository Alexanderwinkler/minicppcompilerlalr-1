
# README

The *MiniCpp compiler LALR(1)* is a modified version of Heinz Dobler's *MiniCpp compiler*. 
More precisely, mainly the compiler's frontend was modified. 
The compiler is implemented in the programming language C# (.NET 4.6.1).

In contrast to Dobler's *MiniCpp compiler* (implemented with the compiler compiler [Coco-2](https://www.researchgate.net/publication/255673320_COCO-2_a_new_compiler_compiler) and uses a LL(1) parsing method),
this *MiniCpp compiler* was impelemented with the compiler compiler [Hime](https://cenotelie.fr/projects/hime/) and uses a LALR(1) parsing method.

---

## The programming language MiniCpp

The programming language *MiniCpp* is a subset of *C++* and supports the following language elements:

* **functions**: with *call-by-value* parameters
* **data types**: `void`, `int`, `bool` and dynamic, one dimensional arrays
* **expressions**: arithmetic, comparison and logical operators
* **statements**: assignment, function call, branching, loop and I/O operations

For more precise information, see **EBNF Grammar for MiniCpp** in this file.

---

## Project structure

The Visual Studio solution has only one project, which can be found in the directory *MiniCppComiplerLalr1*. 
Below you can find the project structur (only the noteworthy files and directories are mentioned):

* **src**: all source files for the *MiniCpp compiler*
* **testfiles**: source files for *MiniCpp* programs (used for testing)
* **tools**: includes *himecc.exe* (used for generating the lexer and the parser) and the *Hime SDK* (in *Hime.SDK.dll* and *Hime.Redist.dll*)
* **MiniCpp.gram**: the *Hime* grammar for *MiniCpp*
* **generate-from-grammar.bat**: used automatically in the build process to generate the lexer and the parser with *Hime*.

---

## How to build

Use *Visual Studio 2017* or higher to build this project. 

To build the project from the command line, use `msbuild` (see [MSBuild CLI reference](https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild-command-line-reference?view=vs-2017)) 
or `dotnet build` (see [.NET Core CLI tools](https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x)) in the root directory .

---

## EBNF Grammar for MiniCpp

The following source code shows the grammar for the programming language MiniCpp. The *MiniCpp compiler LALR(1)* is capable of compiling source files written in this language.

    MiniCpp      = { ConstDecl | VarDef | FuncDecl | FuncDef } ;
    (*----------------------------------------------------------*)
    ConstDecl    = "const", Type, ident, Init, ";" ;
    Init         = "=", ( false | true | [ "+" | "-" ], number ) ;
    VarDef       = Type,  [ "*" ], ident, [ Init ]
                   { ",", [ "*" ] ident, [ Init ] } ";" ;
    FuncDecl     = FuncHead, ";" ;
    FuncDef      = FuncHead, Block ;
    FuncHead     = Type, [ "*" ], ident, 
                   "(", [ FormParList ], ")" ;
    FormParList  = ( "void" |       
                          Type, [ "*" ], ident, [ "[", "]" ]
                    { "," Type, [ "*" ], ident, [ "[", "]" ] } ) ;
    Type         = "void" | "bool" | "int" ;
    Block        = "{", { ConstDecl | VarDef | Stat }, "}" ;
    (*----------------------------------------------------------*)
    Stat         = ( IncStat | DecStat | AssignStat
                    CallStat | IfStat | WhileStat | BreakStat
                  | InputStat | OutputStat | DeleteStat 
                  | ReturnStat
                  | Block | ";" ) ;
    IncStat      = ident, "++", ";" ;
    DecStat      = ident, "--", ";" ;
    AssignStat   = ident, [ "[", Expr, "]"  ], "=", Expr, ";" ;
    CallStat     = ident, "(", [ ActParList ], ")", ";" ;
    ActParList   = Expr, { ",", Expr } ;
    IfStat       = "if", "(", Expr, ")", Stat, [ "else", Stat ] ;
    WhileStat    = "while", "(", Expr, ")", Stat ;
    BreakStat    = "break", ";" ;
    InputStat    = "cin", ">>", ident, ";" ;
    SwitchStat   = "switch", "(", Expr, ")", "{", 
                   { CaseStat }, [ DefaultStat ], "}" ;
    CaseStat     = "case", number, ":", {Stat} ;
    DefaultStat  = "default", ":", { Stat } ;
    OutputStat   = "cout", "<<", 
                         ( Expr | string | "endl" ), 
                  { "<<", ( Expr | string | "endl" ) }, ";" ;
    DeleteStat   = "delete", "[", "]", ident, ";" ;
    ReturnStat   = "return", [ Expr ], ";" ;
    (*----------------------------------------------------------*)
    Expr         = OrExpr ;
    OrExpr       = AndExpr, { "||", AndExpr } ;
    AndExpr      = RelExpr, { "&&", RelExpr } ;
    RelExpr      = SimpleExpr,
                  [ ( "==" | "!=" | "<" | "<=" | ">" | ">=" ), 
                    SimpleExpr ] ;
    SimpleExpr   = [ "+" | "-" ] 
                   Term,    { ( "+" | "-" )        Term    } ;
    Term         = NotFact, { ( "*" | "/" | "%" )  NotFact } ;
    NotFact      = [ "!" ], Fact ;
    Fact         =  "false" | "true"
                  | number  
                  | ident, [   ( "[", Expr,        "]" )
                             | ( "(", [ ActParList ], ")" )
                           ]
                  | "new", Type, "[", Expr, "]"
                  |  "(", Expr, ")" ;