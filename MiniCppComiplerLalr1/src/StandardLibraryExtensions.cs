﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MiniCpp {
  public static class StandardLibraryExtensions {

    public static IEnumerable<T> TakeWithConsume<T>(this Stack<T> stack, int count) {
      var result = new List<T>();
      for (int i = 0; i < count; i++) {
        T elem = stack.Pop();
        result.Add(elem);
      }
      return result;
    }

    public static String TrimUntil(this string str, string until) {
      var index = str.IndexOf("<", StringComparison.Ordinal);
      return index < 0 ? str : str.Substring(0, index);
    }

  }
}