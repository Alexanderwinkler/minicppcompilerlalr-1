// MiniCpp.cs                                                    HDO, 2006-08-28
// ----------                                                    SE,  2009-12-20
// Main program for MiniCpp compiler.
//=====================================|========================================

// *** start: not in Main.frm ***

#define GEN_SRC          // (en|dis)able gen. of source text (with symbol table dump)

#define GEN_CIL_AS_TEXT  // (en|dis)able CIL text generation and assembling to exe
#define GEN_CIL_REF_EMIT // (en|dis)able CIL generation with Reflection.Emit

#define VERIFY_ASSEMBLY  // (en|dis)able CIL verification with PEVerify.exe
#define MEASURE_TIME     // (en|dis)able time measurements for some phases

// *** end ***

using System;
using System.IO;
using Generated;
using Hime.Redist;

namespace MiniCpp {
  public class MiniCpp {

    private static String NAME = "MiniCpp";

    private static void Abort(String abortKind, String moduleName,
      String methName,  String descr) {
      Console.WriteLine();
      Console.WriteLine("*** {0} in module {1} function {2}", 
        abortKind, moduleName, methName);
      Console.WriteLine("*** {0}", descr);
      Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine(NAME + " aborted");
      Utils.Modules(Utils.ModuleAction.cleanupModule);
      Environment.Exit(Utils.EXIT_FAILURE);
    } // Abort

    private static void CompileFile(String srcFileName)
    {
      try {
        using (TextReader srcFile = File.OpenText(srcFileName)) {
          var lex = new MiniCppLexer(srcFile);
          var syn = new MiniCppParser(lex, new MiniCppParserSem());
          NameList.Init(true);
          SymTab.Init();

          Console.WriteLine("parsing           \"" + srcFileName + "\" ...");

          var parseResult = syn.Parse();
          int extStart = srcFileName.LastIndexOf('.');
          String lstFileName = srcFileName.Substring(0, extStart) + ".lst";

          foreach (var error in parseResult.Errors) {
            switch (error.Type) {
              case ParseErrorType.IncorrectUTF16NoHighSurrogate:
                Errors.LexError(error.Position.Line, error.Position.Column, error.Message);
                break;
              case ParseErrorType.IncorrectUTF16NoLowSurrogate:
                Errors.LexError(error.Position.Line, error.Position.Column, error.Message);
                break;
              case ParseErrorType.UnexpectedChar:
                Errors.LexError(error.Position.Line, error.Position.Column, error.Message);
                break;
              case ParseErrorType.UnexpectedEndOfInput:
                Errors.LexError(error.Position.Line, error.Position.Column, error.Message);
                break;
              case ParseErrorType.UnexpectedToken:
                Errors.SynError(error.Position.Line, error.Position.Column, error.Message);
                break;
            }
          }

          if (Errors.NumOfErrors() > 0) {
            Console.WriteLine("{0} error(s) detected,", Errors.NumOfErrors());
            Console.WriteLine("  listing to      \"" + lstFileName + "\"...");
            StreamWriter lst = null;
            try {
              FileStream lstFs = new FileStream(lstFileName, FileMode.Create);
              lst = new StreamWriter(lstFs);
            }
            catch (Exception) {
              lst = null;
            } // try/catch

            if (lst == null) {
              Utils.FatalError(NAME, "CompileFile", "file \"{0}\" not created", lstFileName);
              return;
            } // if

            lst.WriteLine(NAME + " (file: \"{0}\")", srcFileName);
            Errors.GenerateListing(srcFile, lst, Errors.ListingShape.longListing);
            lst.Close();
          }
          else {

            // *** start: not in Main.frm ***

            if (File.Exists(lstFileName))
              File.Delete(lstFileName);

            String moduleName = (String) srcFileName.Clone();
            String path = String.Empty;

            int lastBackSlashIdx = moduleName.LastIndexOf('\\');
            if (lastBackSlashIdx >= 0) {
              path = moduleName.Substring(0, lastBackSlashIdx + 1);
              moduleName = moduleName.Substring(lastBackSlashIdx + 1);
            } // if

            int periodIdx = moduleName.IndexOf('.');
            if (periodIdx >= 0)
              moduleName = moduleName.Substring(0, periodIdx);

#if GEN_SRC // symbol table gen. of source text with symbol table dump
            StartTimer();
            GenSrcText.DumpSymTabAndWriteSrcTxt(path, moduleName);
            WriteElapsedTime("GenSrcText");
#endif

#if GEN_CIL_AS_TEXT // CIL generation to il-file and assembling to exe
            StartTimer();
            GenCilAsText.GenerateAssembly(path, moduleName);
            WriteElapsedTime("GenCilAsText");
            VerifyAssembly(path, moduleName);
#endif // GEN_CIL_AS_TEXT

#if GEN_CIL_REF_EMIT // CIL generation with Reflection.Emit
            StartTimer();
            GenCilByRefEmit.GenerateAssembly(path, moduleName);
            WriteElapsedTime("GenCilByReflectionEmit");
            VerifyAssembly(path, moduleName);
#endif

            // *** end ***

            Console.WriteLine("compilation completed successfully");
          } // else

        }
      } catch (IOException) {
        Utils.FatalError(NAME, "CompileFile", "file \"{0}\" could not be opened", srcFileName);
      } // try/catch

      
      Utils.Modules(Utils.ModuleAction.resetModule);
    } // CompileFile


    // *** start: not in Main.frm ***

    private static readonly System.Diagnostics.Stopwatch Timer = 
      new System.Diagnostics.Stopwatch();
  
    [System.Diagnostics.Conditional("MEASURE_TIME")]
    private static void WriteElapsedTime(string label) {
      Console.WriteLine("elapsed time for '{0}': {1} ms", 
        label, Timer.ElapsedMilliseconds);
    } // WriteElapsedTime

    [System.Diagnostics.Conditional("MEASURE_TIME")]
    private static void StartTimer() {
      Timer.Reset();
      Timer.Start();
    } // StartTimer

    [System.Diagnostics.Conditional("VERIFY_ASSEMBLY")]
    private static void VerifyAssembly(string path, string moduleName) {
      Console.WriteLine();
      Console.WriteLine("PEverifying       \"" + path + moduleName + ".exe ...");
      string fileName = moduleName;
      if (!string.IsNullOrEmpty(path))
        fileName = path + moduleName;
      using (System.Diagnostics.Process p = new System.Diagnostics.Process()) {
        p.StartInfo.FileName =       @"C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.7 Tools\PEVerify.exe";
        if (!File.Exists(p.StartInfo.FileName)) {
          p.StartInfo.FileName =     @"C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\PEVerify.exe";
          if (!File.Exists(p.StartInfo.FileName)) {
            p.StartInfo.FileName =   @"C:\Program Files (x86)\Microsoft SDKs\Windows\v8.0A\bin\NETFX 4.0 Tools\PEVerify.exe";
            if (!File.Exists(p.StartInfo.FileName)) {
              p.StartInfo.FileName = @"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin\NETFX 4.0 Tools\PEVerify.exe";
              if (!File.Exists(p.StartInfo.FileName)) {
                Console.Out.WriteLine("  ERROR: " + p.StartInfo.FileName + " not found");
                return;
              } // else
            } // else
          } // else
        } // else
        p.StartInfo.Arguments = "/NOLOGO " + fileName + ".exe";
        p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardError = true;
        p.StartInfo.RedirectStandardOutput = true;
        try {
          p.Start();
          String output = p.StandardOutput.ReadToEnd();
          p.WaitForExit();
          Console.Out.Write("  " + output);
        } catch (Exception e) {
          Console.Out.WriteLine(e);
        } // catch
      } // using
      Console.WriteLine();
    } // VerifyAssembly

    // *** end ***

    public static void Main(String[] args)
    {
      //-----------------------------------|----------------------------------------

      // --- install modules ---
      Utils.InstallModule("Utils", Utils.UtilsMethod);
      Utils.InstallModule("Errors", Errors.ErrorsMethod);

      // --- initialize modules ---
      Utils.Modules(Utils.ModuleAction.initModule);

      Errors.PushAbortMethod(Abort);

      Console.WriteLine("-------------------------------------");
      Console.WriteLine(" {0} Compiler {1," + (0 - NAME.Length) + "}  V. 4 2018 ", NAME, "");
      Console.WriteLine(" Frontend generated with Hime v3.3.1");
      Console.WriteLine("-------------------------------------");
      Console.WriteLine();

      if (args.Length > 0) { // command line mode
        Console.WriteLine();
        int i = 0;
        do {
          Console.WriteLine("source file       \"{0}\"", args[i]);
          CompileFile(args[i]);
          Console.WriteLine();
          i++;
        } while (i < args.Length);
      } else { // args.Length == 0, interactive mode
        for (; ; ) {
          String srcFileName;
          Utils.GetInputFileName("source file      > ", out srcFileName);
          if (srcFileName.Length > 0) {

            // *** start: not in Main.frm ***
            if (!srcFileName.EndsWith(".mcpp"))
              srcFileName = srcFileName + ".mcpp";
            if (!File.Exists(srcFileName)) {
              srcFileName = ".\\#McppPrograms\\" + srcFileName;
              if (!File.Exists(srcFileName)) {
                srcFileName = "..\\..\\" + srcFileName;
              } // if
            } // if
              // *** end ***

            CompileFile(srcFileName);
          } // if
          char answerCh;
          do {
            Console.WriteLine();
            Console.Write("[c]ontinue or [q]uit > ");
            String answer = Console.ReadLine();
            answerCh = answer.Length == 0 ? ' ' : Char.ToUpper(answer[0]);
          } while (answerCh != 'C' && answerCh != 'Q');
          if (answerCh == 'Q')
            break;
          Console.WriteLine();
        } // for
      } // else

      Utils.Modules(Utils.ModuleAction.cleanupModule);

    } // Main

  }
  
} // MiniCpp

// End of MiniCpp.cs
//=====================================|========================================
