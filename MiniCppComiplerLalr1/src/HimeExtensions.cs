﻿using Hime.Redist;

namespace MiniCpp {
  public static class HimeExtensions {
    public static SemanticElement GetLastElement(this SemanticBody body) {
      return body[body.Length - 1];
    }

    public static SemanticElement GetElementReverse(this SemanticBody body, int pos) {
      return body[body.Length - 1 - pos];
    }
  }
}