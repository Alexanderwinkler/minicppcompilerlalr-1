// Utils.cs                                       HDO, 2006-08-28; SN, 10.9.2007
// --------
// Collection of useful utilities (constants, types, and methods).
//=====================================|========================================

#undef TEST_UTILS

using System;

namespace MiniCpp {
  public class Utils {

    public const String MODULENAME = "Utils";

    public static void UtilsMethod(ModuleAction action, out String moduleName) {
      //-----------------------------------|----------------------------------------
      moduleName = MODULENAME;
      switch (action) {
        case ModuleAction.getModuleName:
          return;
        case ModuleAction.initModule:
          timerRuns = false;
          break;
        case ModuleAction.resetModule:
          timerRuns = false;
          break;
        case ModuleAction.cleanupModule:
          return;
      } // switch
    } // UtilsMethod

    public const int EXIT_FAILURE = -1;
    public const int EXIT_SUCCESS = 0;

    // --- module handling types and program module handling ---

    public enum ModuleAction {
      getModuleName,
      initModule,
      resetModule,
      cleanupModule
    } // ModuleAction

    public delegate void ModuleMethodDelegate(ModuleAction action,
      out String moduleName);

    private struct ModuleInfo {
      public String moduleName;
      public ModuleMethodDelegate moduleMethod;
      public bool init;
    } // ModuleInfo

    private const int MAXMODULES = 20;
    private static ModuleInfo[] mil = new ModuleInfo[MAXMODULES];
    private static int modCnt;

    public static void InstallModule(String moduleName, ModuleMethodDelegate moduleMethod) {
      //-----------------------------------|----------------------------------------
      if (moduleName == MODULENAME) {
        modCnt = 0;
        for (int i = 0; i < MAXMODULES; i++) {
          mil[i].moduleName = "";
          mil[i].moduleMethod = null;
          mil[i].init = false;
        } // for
      } // if
      if (modCnt == MAXMODULES)
        FatalError(MODULENAME, "InstallModule", "too many modules");
      String mn;
      moduleMethod(ModuleAction.getModuleName, out mn);
      if (moduleName != mn)
        FatalError(MODULENAME, "InstallModule",
          "incorrect module name \"{0}\"", moduleName);
      mil[modCnt].moduleName = moduleName;
      mil[modCnt].moduleMethod = moduleMethod;
      mil[modCnt].init = false;
      modCnt++;
    } // InstallModule

    private static bool withinCleanUp = false;

    public static void Modules(ModuleAction action) {
      //-----------------------------------|----------------------------------------
      String dummy;
      switch (action) {
        case ModuleAction.initModule:
          for (int i = 0; i < modCnt; i++) {
            if (!mil[i].init) {
              mil[i].moduleMethod(ModuleAction.initModule, out dummy);
              mil[i].init = true;
            } else
              FatalError(MODULENAME, "Modules",
                "{0} reinitialized", mil[i].moduleName);
          } // for
          break;
        case ModuleAction.resetModule:
        case ModuleAction.cleanupModule:
          if (!withinCleanUp) {
            withinCleanUp = true;
            for (int i = modCnt - 1; i >= 0; i--)
              if (mil[i].init) {
                mil[i].moduleMethod(action, out dummy);
                mil[i].init = action != ModuleAction.cleanupModule;
              } else
                FatalError(MODULENAME, "Modules",
                  "{0} not initialized", mil[i].moduleName);
            withinCleanUp = false;
          } // if
          break;
        default:
          FatalError(MODULENAME, "Modules",
            "invalid ModuleAction {0}", action.ToString());
          break;
      } // switch
    } // Modules


    // --- misc. utilities ---

    public static void FatalError(String moduleName, String methodName,
      String fmt, params Object[] p) {
      //-----------------------------------|----------------------------------------
      Console.WriteLine();
      Console.WriteLine("*** FATAL ERROR in module {0}, method {1}",
        moduleName, methodName);
      Console.WriteLine("*** " + fmt, p);
      Modules(ModuleAction.cleanupModule);
      Environment.Exit(EXIT_FAILURE);
    } // FatalError

    public static void GetInputFileName(String prompt, out String fileName) {
      //-----------------------------------|----------------------------------------
      Console.WriteLine();
      Console.Write("{0}", prompt);
      fileName = Console.ReadLine();
    } // GetInputFileName


    // --- timer utilities ---

    private static bool timerRuns = false;
    private static DateTime startedAt, stoppedAt;

    public static void StartTimer() {
      //-----------------------------------|----------------------------------------
      if (timerRuns)
        FatalError(MODULENAME, "StartTimer", "timer still running");
      timerRuns = true;
      startedAt = DateTime.Now;
    } // StartTimer

    public static void StopTimer() {
      //-----------------------------------|----------------------------------------
      if (!timerRuns)
        FatalError(MODULENAME, "StopTimer", "timer not running");
      stoppedAt = DateTime.Now;
      timerRuns = false;
    } // StopTimer

    public static TimeSpan ElapsedTime() {
      //-----------------------------------|----------------------------------------
      TimeSpan elapsed = stoppedAt - startedAt;
      return elapsed;
    } // ElapsedTime

#if TEST_UTILS

  public static void Main(String[] args) {
    Console.WriteLine("START: Utils");

    Console.WriteLine("installing ...");
    InstallModule("Utils", new ModuleMethodDelegate(UtilsMethod));
    Console.WriteLine("initModule ...");
    Modules(ModuleAction.initModule);

    // FatalError(MODULENAME, "Main", 
    //            "any {0} error {1}", "additional", "messages");

    Console.WriteLine("StartTimer ...");
    StartTimer();
    String fn;
    GetInputFileName("input file name > ", out fn);
    Console.WriteLine("input file name = " + fn);
    Console.WriteLine("StopTimer ...");
    StopTimer();
    Console.WriteLine(ElapsedTime());

    Console.WriteLine("resetModule ...");
    Modules(ModuleAction.resetModule);

    Console.WriteLine("cleanupModule ...");
    Modules(ModuleAction.cleanupModule);

    Console.WriteLine("END");
    // Console.WriteLine("type [CR] to continue ...");
    // Console.ReadLine();
  } // Main

#endif

  }
} // Utils

// End of Utils.cs
//=====================================|========================================


