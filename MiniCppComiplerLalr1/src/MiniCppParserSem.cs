﻿// GenSrcText.cs:                                  Alexander Winkler, 2018-20-04
// -------------
//
// Semantic evaluator for bottom-up parsing.
//=====================================|========================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Generated;
using Hime.Redist;

namespace MiniCpp {
  public class MiniCppParserSem : MiniCppParser.Actions {

    private int loopLevel = 0;

    #region Non terminal result stacks
    private readonly Stack<Stat> constDeclStack = new Stack<Stat>();
    private readonly Stack<Tuple<int, Type>> initStack = new Stack<Tuple<int, Type>>();
    private readonly Stack<Stat> varDefStack = new Stack<Stat>();
    private readonly Stack<Symbol> funcHeadStack = new Stack<Symbol>();
    private readonly Stack<Type> typeStack = new Stack<Type>();
    private readonly Stack<Tuple<Symbol, Stat>> blockStack = new Stack<Tuple<Symbol, Stat>>();

    private readonly Stack<Stat> statStack = new Stack<Stat>();
    private readonly Stack<Stat> incStatStack = new Stack<Stat>();
    private readonly Stack<Stat> decStatStack = new Stack<Stat>();
    private readonly Stack<Stat> assignStatStack = new Stack<Stat>();

    private readonly Stack<Expr> actParListStack = new Stack<Expr>();
    private readonly Stack<Expr> exprStack = new Stack<Expr>();
    private readonly Stack<Expr> factStack = new Stack<Expr>();
    private readonly Stack<Expr> notFactStack = new Stack<Expr>();
    private readonly Stack<Expr> termStack = new Stack<Expr>();
    private readonly Stack<Expr> simpleExprStack = new Stack<Expr>();
    private readonly Stack<Expr> relExprStack = new Stack<Expr>();
    private readonly Stack<Expr> andExprStack = new Stack<Expr>();
    private readonly Stack<Expr> orExprStack = new Stack<Expr>();

    private readonly Stack<Stat> ifStatStack = new Stack<Stat>();
    private readonly Stack<Stat> whileStatStack = new Stack<Stat>();
    private readonly Stack<Stat> breakStatStack = new Stack<Stat>();
    private readonly Stack<Stat> inputStatStack = new Stack<Stat>();
    private readonly Stack<Stat> outputStatStack = new Stack<Stat>();
    private readonly Stack<Stat> deleteStatStack = new Stack<Stat>();
    private readonly Stack<Stat> returnStatStack = new Stack<Stat>();

    private readonly Stack<Stat> switchStatStack = new Stack<Stat>();
    private readonly Stack<Stat> caseStatStack = new Stack<Stat>();
    private readonly Stack<Stat> defaultStatStack = new Stack<Stat>();

    private readonly Stack<Stat> callStatStack = new Stack<Stat>();
    #endregion

    public override void OnMiniCppAfter(Hime.Redist.Symbol head, SemanticBody body) {
      if (!SymTab.MainFuncDefined()) {
        SemErr("no main func defined", new SrcPos(new TextPosition(0, 0)));
      }
    }

    public override void OnConstDecl(Hime.Redist.Symbol head, SemanticBody body) {
      var type = typeStack.Pop();
      var init = initStack.Pop();
      var identToken = body[2].Value;
      var identSpix = NameList.SpixOf(identToken);
      
      var sy = SymTab.Insert(new SrcPos(body[2].Position),  identSpix, Symbol.Kind.constKind, type, false);

      var constDecl = CreateAssignStat(sy, init.Item1, init.Item2, new SrcPos(body[0].Position));
      constDeclStack.Push(constDecl);
    }

    public override void OnInit(Hime.Redist.Symbol head, SemanticBody body) {
      var token1 = body[1].Value;
      int resultValue;
      Type resultType;
      switch (token1) {
        case "false":
          resultValue = 0;
          resultType = Type.boolType;
          break;
        case "true":
          resultValue = 1;
          resultType = Type.boolType;
          break;
        default:  // "+" || "-"
          resultType = Type.intType;
          switch (body[2].Value) {
            case "+":
              resultValue = Convert.ToInt32(body[2].Value);
              break;
            case "-":
              resultValue = Convert.ToInt32(body[2].Value) * -1;
              break;
            default:
              resultValue = Convert.ToInt32(body[1].Value);
              break;
          }
          break;
      }
      initStack.Push(new Tuple<int, Type>(resultValue, resultType));
    }

    public override void OnVarDef(Hime.Redist.Symbol head, SemanticBody body) {
      int pos = 1;
      var type = typeStack.Pop();

      var ptr = false;
      if (body[pos].Value == "*") {
        ptr = true;
        pos++;
      }
      var ident = body[pos].Value;
      var identSpix = NameList.SpixOf(ident);
      var sy = SymTab.Insert(new SrcPos(body[pos].Position),  identSpix, Symbol.Kind.varKind, type, ptr);
      pos++;
      //var prevSy = sy;
      //var firstSy = sy;
      Stat assignStat = null;
      Stat firstAssignStat = null;
      Stat prevAssignStat = null;

      if (body[pos].Symbol.Name == "Init") {
        var init = initStack.Pop();
        sy.init = true;
        assignStat = CreateAssignStat(sy, init.Item1, init.Item2, new SrcPos(body[0].Position));
        firstAssignStat = assignStat;
        prevAssignStat = assignStat;
        pos++;
      }
      pos++;

      while (pos < body.Length) {
        ptr = false;
        if (body[pos].Value == "*") {
          ptr = true;
          pos++;
        }
        var srcPos = new SrcPos(body[pos].Position);
        ident = body[pos].Value;
        identSpix = NameList.SpixOf(ident);
        sy = SymTab.Insert(new SrcPos(body[pos].Position), identSpix, Symbol.Kind.varKind, type, ptr);
        pos++;
        //prevSy.next = sy;
        //prevSy = sy;

        if (body[pos].Symbol.Name == "Init") {
          var init = initStack.Pop();
          sy.init = true;
          assignStat = CreateAssignStat(sy, init.Item1, init.Item2, srcPos);
          if (prevAssignStat != null) {
            prevAssignStat.next = assignStat;
          } else {
            firstAssignStat = assignStat;
          }
          prevAssignStat = assignStat;
          pos++;
        }
        pos++;
        
      }
      varDefStack.Push(firstAssignStat);
    }

    public override void OnFuncDecl(Hime.Redist.Symbol head, SemanticBody body) {
      var funcHead = funcHeadStack.Pop();
      if (funcHead.kind == Symbol.Kind.funcKind) {
        if (funcHead.hadFuncDecl)
          SemErr("multiple function declaration", new SrcPos(body[0].Position));
        else {
          funcHead.hadFuncDecl = true;
          funcHead.funcDeclParList = SymTab.CurSymbols();
        } // else
      } // if
      SymTab.LeaveScope();
    }

    public override void OnFuncDefHead(Hime.Redist.Symbol head, SemanticBody body) {
      var funcHead = funcHeadStack.Peek();
      funcHead.symbols = SymTab.CurSymbols();
      funcHead.FuncDef(new SrcPos(body[0].Position));
    }


    public override void OnFuncDef(Hime.Redist.Symbol head, SemanticBody body) {
      var funcHead = funcHeadStack.Pop();

      var block = blockStack.Pop();
      funcHead.symbols = block.Item1;
      funcHead.statList = block.Item2;
      SymTab.LeaveScope();
    }

    public override void OnFuncHeadReturnType(Hime.Redist.Symbol head, SemanticBody body) {
      int pos = 1;
      var type = typeStack.Pop();
      bool ptr;
      if (body[pos].Value == "*") {
        ptr = true;
        pos++;
      } else {
        ptr = false;
      }
      var ident = body[pos].Value;
      var identSpix = NameList.SpixOf(ident);

      var funcSy = SymTab.Lookup(identSpix);
      if (funcSy == null)
        funcSy = SymTab.Insert(new SrcPos(body[pos].Position),  identSpix, Symbol.Kind.funcKind, type, ptr);
      else if (funcSy.kind != Symbol.Kind.undefKind) {
        if (funcSy.kind == Symbol.Kind.funcKind &&
            funcSy.defined)
          SemErr("multiple function decl or def", new SrcPos(body[pos].Position));
        else if (funcSy.kind != Symbol.Kind.funcKind) {
          SemErr("invalid redefinition", new SrcPos(body[pos].Position));
          funcSy.kind = Symbol.Kind.undefKind;
        }
      }
      funcHeadStack.Push(funcSy);
      SymTab.EnterScope();
      //curFuncSy = funcSy;
    }

    public override void OnFormParList(Hime.Redist.Symbol head, SemanticBody body) {
      
      if (body.Length == 1) {
        // nothing to do
      } else {
        var pos = 1;
        while (pos < body.Length) {
          var type = typeStack.Pop();
          bool ptr;
          if (body[pos].Value == "*") {
            ptr = true;
            pos++;
          } else {
            ptr = false;
          }
          var ident = body[pos].Value;
          var identSpix = NameList.SpixOf(ident);
          var sp = new SrcPos(body[pos].Position);
          pos++;
          if (pos < body.Length && body[pos].Value == "[") {
            if (ptr) {
              SemErr("pointer to array not supported", new SrcPos(body[pos].Position));
            }
            ptr = true;
            pos++;
          }
          SymTab.Insert(sp, identSpix, Symbol.Kind.parKind, type, ptr);
          pos += 2;
        }
      }
    }

    public override void OnTypeVoid(Hime.Redist.Symbol head, SemanticBody body) {
      typeStack.Push(Type.voidType);
    }

    public override void OnTypeBool(Hime.Redist.Symbol head, SemanticBody body) {
      typeStack.Push(Type.boolType);
    }

    public override void OnTypeInt(Hime.Redist.Symbol head, SemanticBody body) {
      typeStack.Push(Type.intType);
    }

    public override void OnStatBlock(Hime.Redist.Symbol head, SemanticBody body) {
      var blockStatList = blockStack.Pop().Item2;
      statStack.Push(new BlockStat(new SrcPos(body[0].Position), blockStatList));
    }

    public override void OnStatBreakStat(Hime.Redist.Symbol head, SemanticBody body) {
      statStack.Push(breakStatStack.Pop());
    }

    public override void OnBlock(Hime.Redist.Symbol head, SemanticBody body) {
      var statList = new Stack<Stat>(body.Length - 2);
      for (int i = body.Length - 2; i > 0 ; i--) {
        var currentSymbolName = body[i].Symbol.Name;
        currentSymbolName = currentSymbolName.TrimUntil("<");
        switch (currentSymbolName) {
          case "Stat":
            statList.Push(statStack.Pop());
            break;
          case "StatWithBreak":
            statList.Push(statStack.Pop());
            break;
          case "ConstDecl":
            var constDef = constDeclStack.Pop();
            if (constDef != null && loopLevel > 0) {
              statList.Push(constDef);
            }
            break;
          case "VarDef":
            var varDef = varDefStack.Pop();
            if (varDef != null && loopLevel > 0) { 
              statList.Push(varDef);
            }
            break;
        }
      }

      Stat first = null;
      if (statList.Count > 0) {
        statList.Aggregate((acc, cur) => acc.next = cur);
        first = statList.Pop();
      }


      blockStack.Push(new Tuple<Symbol, Stat>(
        SymTab.CurSymbols(),
        first)); 
    }

    

    public override void OnBaseStat(Hime.Redist.Symbol head, SemanticBody body) {
      var symbolName = body[0].Symbol.Name;
      switch (symbolName) {
        case "IncStat":
          statStack.Push(incStatStack.Pop());
          break;
        case "DecStat":
          statStack.Push(decStatStack.Pop());
          break;
        case "AssignStat":
          statStack.Push(assignStatStack.Pop());
          break;
        case "CallStat":
          statStack.Push(callStatStack.Pop());
          break;
        case "InputStat":
          statStack.Push(inputStatStack.Pop());
          break;
        case "OutputStat":
          statStack.Push(outputStatStack.Pop());
          break;
        case "DeleteStat":
          statStack.Push(deleteStatStack.Pop());
          break;
        case "ReturnStat":
          statStack.Push(returnStatStack.Pop());
          break;
        case "SwitchStat":
          statStack.Push(switchStatStack.Pop());
          break;
        case ";":
          statStack.Push(new EmptyStat(null));
          break;
          ;
      }
    }

    public override void OnStat(Hime.Redist.Symbol head, SemanticBody body) {
      var symbolName = body[0].Symbol.Name;
      symbolName = symbolName.TrimUntil("<");
      switch (symbolName) {
        case "WhileStat":
          statStack.Push(whileStatStack.Pop());
          break;
        case "WhileStatThen":
          statStack.Push(whileStatStack.Pop());
          break;
        case "IfStatIncomplete":
          statStack.Push(ifStatStack.Pop());
          break;
        case "IfStatComplete":
          statStack.Push(ifStatStack.Pop());
          break;
          // base stat not needed, since all stack operations are handled in OnBaseStat
      }
    }

    public override void OnIncStat(Hime.Redist.Symbol head, SemanticBody body) {
      var identToken = body[0].Value;
      var identSpix = NameList.SpixOf(identToken);
      var sp = new SrcPos(body[0].Position);
      var sy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.varKind, Symbol.Kind.parKind, Symbol.Kind.funcKind);
      incStatStack.Push(new IncStat(sp, new VarOperand(sp, sy)));
    }

    public override void OnDecStat(Hime.Redist.Symbol head, SemanticBody body) {
      var identToken = body[0].Value;
      var identSpix = NameList.SpixOf(identToken);
      var sp = new SrcPos(body[0].Position);
      var sy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.varKind, Symbol.Kind.parKind, Symbol.Kind.funcKind);
      decStatStack.Push(new DecStat(sp, new VarOperand(sp, sy)));
    }

    public override void OnAssignStat(Hime.Redist.Symbol head, SemanticBody body) {
      var identToken = body[0].Value;
      var identSpix = NameList.SpixOf(identToken);
      var sp = new SrcPos(body[0].Position);
      var sy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.varKind, Symbol.Kind.parKind, Symbol.Kind.funcKind);
      Expr lhs = null;
      Expr rhs = null;
      rhs = exprStack.Pop();
      if (body[1].Value == "[") {
        lhs = new ArrIdxOperator(sp, new VarOperand(sp, sy), exprStack.Pop());
      } else {
        lhs = new VarOperand(sp, sy);
      }
      var result = new AssignStat(new SrcPos(body[0].Position), lhs, rhs);
      assignStatStack.Push(result);
    }

    public override void OnCallStat(Hime.Redist.Symbol head, SemanticBody body) {
      var ident = body[0].Value;
      var identSpix = NameList.SpixOf(ident);
      var sp = new SrcPos(body[0].Position);
      var funcSy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.funcKind);
      var actParList = body[2].Symbol.Name == "ActParList" ? actParListStack.Pop() : null;
      callStatStack.Push(new CallStat(new SrcPos(body[0].Position), funcSy, actParList));
    }

    public override void OnActParList(Hime.Redist.Symbol head, SemanticBody body) {
      Expr expr = null;
      Expr prevExpr = null;

      for (int i = 0; i < body.Length; i+=2) {
        expr = exprStack.Pop();
        if (prevExpr == null) {
          prevExpr = expr;
        } else {
          expr.next = prevExpr;
          prevExpr = expr;
        }
      }
      actParListStack.Push(expr);
    }

    public override void OnIfStat(Hime.Redist.Symbol head, SemanticBody body) {
      var expr = exprStack.Pop();
      Stat thenStat = statStack.Pop();

      var ifStat = new IfStat(new SrcPos(body[0].Position), expr, thenStat, null);
      ifStatStack.Push(ifStat);
    }

    public override void OnIfStatElse(Hime.Redist.Symbol head, SemanticBody body)
    {
      var expr = exprStack.Pop();
      Stat elseStat = statStack.Pop();
      Stat thenStat = statStack.Pop();

      var ifStat = new IfStat(new SrcPos(body[0].Position), expr, thenStat, elseStat);
      ifStatStack.Push(ifStat);
    }

    public override void OnWhileStatBegin(Hime.Redist.Symbol head, SemanticBody body) {
      loopLevel++;
    }

    public override void OnWhileStat(Hime.Redist.Symbol head, SemanticBody body) {
      var expr = exprStack.Pop();
      var stat = statStack.Pop();
      var whileStat = new WhileStat(new SrcPos(body[0].Position), expr, stat);
      loopLevel--;
      whileStatStack.Push(whileStat);
    }

    public override void OnBreakStat(Hime.Redist.Symbol head, SemanticBody body) {
      var breakStat = new BreakStat(new SrcPos(body[0].Position));
      breakStatStack.Push(breakStat);
    }

    public override void InputStat(Hime.Redist.Symbol head, SemanticBody body) {
      var identToken = body[2].Value;
      var identSpix = NameList.SpixOf(identToken);
      var sp = new SrcPos(body[2].Position);
      var sy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.varKind, Symbol.Kind.parKind);
      var result = new InputStat(new SrcPos(body[0].Position), new VarOperand(sp, sy));
      inputStatStack.Push(result);
    }

    public override void OnSwitchStat(Hime.Redist.Symbol head, SemanticBody body) {
      var expr = exprStack.Pop();
      var switchStat = new SwitchStat(expr);
      for (int i = body.Length - 3; i > 4; i--) {
        switchStat.AddCase(caseStatStack.Pop());
      }
      if (body.GetElementReverse(1).Symbol.Name == "DefaultStat") {
        switchStat.AddCase(defaultStatStack.Pop());
      }
      switchStatStack.Push(switchStat);
    }

    public override void OnCaseStat(Hime.Redist.Symbol head, SemanticBody body) {
      var number = Convert.ToInt32(body[1].Value);
      Stat stat = null;
      Stat prevStat = null;
      for (int i = body.Length - 1; i > 2; i--) {
        stat = statStack.Pop();
        if (prevStat == null) {
          prevStat = stat;
        } else {
          stat.next = prevStat;
          prevStat = stat;
        }
      }
      caseStatStack.Push(new CaseStat(number, stat));
    }

    public override void OnDefaultStat(Hime.Redist.Symbol head, SemanticBody body) {
      Stat stat = null;
      Stat prevStat = null;
      for (int i = body.Length - 1; i > 1; i--) {
        stat = statStack.Pop();
        if (prevStat == null) {
          prevStat = stat;
        } else {
          stat.next = prevStat;
          prevStat = stat;
        }
      }
      defaultStatStack.Push(stat);
    }

    public override void OnFactIdent(Hime.Redist.Symbol head, SemanticBody body) {
      var identToken = body[0].Value;
      var identSpix = NameList.SpixOf(identToken);
      var sp = new SrcPos(body[0].Position);
      if (body.Length == 1) {
        var sy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.constKind, Symbol.Kind.varKind, Symbol.Kind.parKind);
        var f = new VarOperand(sp,  sy);
        factStack.Push(f);
      } else if (body[1].Value == "[") {
        var sy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.varKind, Symbol.Kind.parKind);
        var expr = exprStack.Pop();
        var f = new ArrIdxOperator(sp, new VarOperand(new SrcPos(body[0].Position), sy), expr);
        factStack.Push(f);
      } else if (body[1].Value == "(") {
        Expr factParList = null;
        var sy = SymTab.SymbolOf(sp, identSpix, Symbol.Kind.funcKind);
        if (body[2].Value != ")") {
          factParList = actParListStack.Pop();
        }
        var f = new FuncCallOperator(sp, sy, factParList);
        factStack.Push(f);
      }
    }

    public override void OnFactNewArray(Hime.Redist.Symbol head, SemanticBody body) {
      var t = typeStack.Pop();
      var e = exprStack.Pop();
      var f = new NewOperator(new SrcPos(body[0].Position), t, e);
      factStack.Push(f);
    }

    public override void OnFactParanthesisExpr(Hime.Redist.Symbol head, SemanticBody body) {
      var f = exprStack.Pop();
      factStack.Push(f);
    }

    public override void OnOutputStatResult(Hime.Redist.Symbol head, SemanticBody body) {
      var values = new Stack(); //TODO (optimization): init with precomputed size
      for (int i = body.Length - 1; i >= 2; i -= 2) {
        var currentElement = body[i];
        switch (currentElement.Symbol.Name) {
          case "Expr":
            values.Push(exprStack.Pop());
            break;
          case "string":
            var str = currentElement.Value.Substring(1, currentElement.Value.Length - 2);
            values.Push(str);
            break;
          case "endl":
            values.Push("\n");
            break;
        }
      }
      var outputStatResult = new OutputStat(new SrcPos(body[0].Position), values);
      outputStatStack.Push(outputStatResult);
    }

    public override void OnDeleteStat(Hime.Redist.Symbol head, SemanticBody body) {
      var identToken = body[3].Value;
      var identSpix = NameList.SpixOf(identToken);
      var sy = SymTab.SymbolOf(new SrcPos(body[3].Position), identSpix, Symbol.Kind.varKind, Symbol.Kind.parKind);
      var sp = new SrcPos(body[0].Position);
      var result = new DeleteStat(sp, new VarOperand(sp, sy));
      deleteStatStack.Push(result);
    }

    public override void ReturnStat(Hime.Redist.Symbol head, SemanticBody body) {
      var expr = body[1].Symbol.Name == "Expr" ? exprStack.Pop() : null;
      returnStatStack.Push(new ReturnStat(new SrcPos(body[0].Position), funcHeadStack.Peek(), expr));
    }

    #region Expression returning rules

    public override void OnExprResult(Hime.Redist.Symbol head, SemanticBody body) {
      exprStack.Push(orExprStack.Pop());
    }

    public override void OnOrExprResult(Hime.Redist.Symbol head, SemanticBody body) {
      if (body.Length == 1) {
        orExprStack.Push(andExprStack.Pop());
      } else {
        int additionalAndExprCnt = (body.Length - 1) / 2;
        var additionalAndExprs = andExprStack
          .TakeWithConsume(additionalAndExprCnt)
          .Reverse();
        var firstAndExpr = andExprStack.Pop();
        var orExprResult = additionalAndExprs
          .Aggregate(firstAndExpr, (acc, cur) =>
            new BinaryOperator(new SrcPos(body[0].Position), BinaryOperator.Operation.orOp, acc, cur));
        orExprStack.Push(orExprResult);
      }
    }

    public override void OnAndExprResult(Hime.Redist.Symbol head, SemanticBody body) {
      if (body.Length == 1) {
        andExprStack.Push(relExprStack.Pop());
      } else {
        int additionalRelExprCnt = (body.Length - 1) / 2;

        var additionalRelExprs = relExprStack
          .TakeWithConsume(additionalRelExprCnt)
          .Reverse();
        var firstRelExpr = relExprStack.Pop();
        var andExprResult = additionalRelExprs
          .Aggregate(firstRelExpr, (acc, cur) => 
            new BinaryOperator(new SrcPos(body[0].Position), BinaryOperator.Operation.andOp, acc, cur));
        andExprStack.Push(andExprResult);
      }
    }

    public override void OnRelExprResult(Hime.Redist.Symbol head, SemanticBody body) {
      if (body.Length == 1) {
        relExprStack.Push(simpleExprStack.Pop());
      } else {
        var binOpToken = body[1].Value;
        var binOp = BinaryOperator.Operation.undefOp;
        switch (binOpToken) {
          case "==":
            binOp = BinaryOperator.Operation.eqOp;
            break;
          case "!=":
            binOp = BinaryOperator.Operation.neOp;
            break;
          case "<":
            binOp = BinaryOperator.Operation.ltOp;
            break;
          case "<=":
            binOp = BinaryOperator.Operation.leOp;
            break;
          case ">":
            binOp = BinaryOperator.Operation.gtOp;
            break;
          case ">=":
            binOp = BinaryOperator.Operation.geOp;
            break;
        }
        var right = simpleExprStack.Pop();
        var left = simpleExprStack.Pop();
        relExprStack.Push(new BinaryOperator(new SrcPos(body[0].Position), binOp, left, right));
      }
    }

    public override void OnSimpleExprResult(Hime.Redist.Symbol head, SemanticBody body) {
      int additionalTermsStartPos;
      UnaryOperator.Operation op;
      if (body[0].Value == "+") {
        op = UnaryOperator.Operation.posOp;
        additionalTermsStartPos = 2;
      } else if (body[0].Value == "-") {
        op = UnaryOperator.Operation.negOp;
        additionalTermsStartPos = 2;
      } else {
        additionalTermsStartPos = 1;
        op = UnaryOperator.Operation.undefOp;
      }

      int additionalTermsCnt = (body.Length - additionalTermsStartPos) / 2;
      var terms = termStack
        .TakeWithConsume(additionalTermsCnt);
      var firstTerm = termStack.Pop();
      if (op != UnaryOperator.Operation.undefOp) {
        firstTerm = new UnaryOperator(new SrcPos(body[0].Position), op, firstTerm);
      }

      Stack<Tuple<BinaryOperator.Operation, TextPosition>> binOps = new Stack<Tuple<BinaryOperator.Operation, TextPosition>>();
      for (int i = additionalTermsStartPos; i < body.Length; i += 2) {
        binOps.Push(body[i].Value == "+" 
          ? new Tuple<BinaryOperator.Operation, TextPosition>(BinaryOperator.Operation.addOp, body[i].Position)
          : new Tuple<BinaryOperator.Operation, TextPosition>(BinaryOperator.Operation.subOp, body[i].Position));
      }

      var simpleExprResult = binOps
        .Zip(terms, (operation, expr) => new {operation, expr})
        .Reverse() // reverse to get them in the order of the parser's read direction (left to right)
        .Aggregate(firstTerm, (acc, cur) =>
          new BinaryOperator(new SrcPos(cur.operation.Item2), cur.operation.Item1, acc, cur.expr));
      simpleExprStack.Push(simpleExprResult);
    }

    public override void OnTermResult(Hime.Redist.Symbol head, SemanticBody body) {
      int additionalNotFactsCnt = (body.Length - 1) / 2;
      var notFacts = notFactStack
        .TakeWithConsume(additionalNotFactsCnt);
      var firstNotFact = notFactStack.Pop();

      Stack<Tuple<BinaryOperator.Operation, TextPosition>> binOps = new Stack<Tuple<BinaryOperator.Operation, TextPosition>>();
      for (int i = 1; i < body.Length; i += 2) {
        switch (body[i].Value) {
          case "*":
            binOps.Push(new Tuple<BinaryOperator.Operation, TextPosition>(BinaryOperator.Operation.mulOp, body[i].Position));
            break;
          case "/":
            binOps.Push(new Tuple<BinaryOperator.Operation, TextPosition>(BinaryOperator.Operation.divOp, body[i].Position));
            break;
          case "%":
            binOps.Push(new Tuple<BinaryOperator.Operation, TextPosition>(BinaryOperator.Operation.modOp, body[i].Position));
            break;
        }
      }

      var termResult = binOps
        .Zip(notFacts, (operation, expr) => new { operation, expr })
        .Reverse() // reverse to get them in the order of the parser's read direction (left to right)
        .Aggregate(firstNotFact, (acc, cur) =>
          new BinaryOperator(new SrcPos(cur.operation.Item2), cur.operation.Item1, acc, cur.expr));
      termStack.Push(termResult);
    }

    public override void OnNotFact(Hime.Redist.Symbol head, SemanticBody body) {
      bool hasNot = body.Length > 1;
      var f = factStack.Pop();
      var nf = hasNot ? 
        new UnaryOperator(new SrcPos(body[0].Position), UnaryOperator.Operation.notOp, f) 
        : f;
      notFactStack.Push(nf);
    }

    public override void OnFactFalse(Hime.Redist.Symbol head, SemanticBody body) {
      var f = new LitOperand(Type.boolType, 0);
      factStack.Push(f);
    }

    public override void OnFactTrue(Hime.Redist.Symbol head, SemanticBody body) {
      var f = new LitOperand(Type.boolType, 1);
      factStack.Push(f);
    }

    public override void OnFactNumber(Hime.Redist.Symbol head, SemanticBody body) {
      var number = Convert.ToInt32(body[0].Value);
      var f = new LitOperand(Type.intType, number);
      factStack.Push(f);
    }

    #endregion

    private AssignStat CreateAssignStat(Symbol sy, int value, Type expectedType, SrcPos position) {
      AssignStat result = null;

      sy.val = value;
      if (expectedType == Type.boolType) {
        if (sy.type != Type.boolType) {
          SemErr("Invalid Type", position);
        }
      } else if (expectedType == Type.intType) {
        if (sy.type.IsPtrType() && value != 0) {
          SemErr("invalid value", position);
        }
      }
      else if (sy.type.kind != Type.Kind.intKind) {
        SemErr("invalid type", position);
      }
      

      if (sy.kind == Symbol.Kind.varKind) {
        result = new AssignStat(position, new VarOperand(position, sy), new LitOperand(sy.type, sy.val));
      }
      return result;
    }

    private static void SemErr(string msg, SrcPos pos)
    {
      Errors.SemError(pos.line, pos.col, msg);
    }

  }
}