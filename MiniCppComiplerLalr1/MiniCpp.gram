grammar MiniCpp
{
    options
    {
        Axiom = "MiniCpp";
        Separator = "seperator";
		OutputPath = "./src/Generated";
        Namespace = "Generated";
        //ParserType = "RNGLR";
        AccessModifier = "Public";
    }

    terminals
    {
    //  ----------------------------------------------------------------------------
    //  CHARACTER SETS
    //  ----------------------------------------------------------------------------
        letter              ->  [a-zA-Z_] ;
        digit               ->  [0-9] ;
        stringChar          ->  [U+0020 .. U+0021]      // ' ' .. '!'
                            | [U+0023 .. U+007E] ;    // '#' .. '~'
    //  ----------------------------------------------------------------------------
    //  COMMENTS
    //  ----------------------------------------------------------------------------
        space               ->  U+0020 | U+0009 ;
        linebreak           ->  U+000A | U+000D | U+0015
                            | U+000C | U+0085 | U+2028 | U+2029 ;
        whiteSpace          ->  space | linebreak ; 
        comment             ->  ('/*' ( .* - (.* '*/' .*) ) '*/')
                                | '//' (.* - (.* linebreak .*)) linebreak ;
        seperator           ->  (space | linebreak | comment);
    //  ----------------------------------------------------------------------------
    //  TOKEN CLASSES
    //  ----------------------------------------------------------------------------
        ident               ->  letter (letter | digit)*;
        number              ->  digit (digit)* ;
        string              ->  '"'  ( (. - ('"' | '\\' | linebreak)))* '"' ;
    }

    rules
    {
        MiniCpp             ->  ( ConstDecl | VarDef | FuncDecl | FuncDef )* 
                                @OnMiniCppAfter ;
        Keyword            -> 'bool' | 'break' | 'cin' | 'const' | 'cout' | 'delete'
                            | 'else' | 'endl' | 'false' | 'if' | 'int' | 'new' 
                            | 'return' | 'true' | 'void' | 'while' | 'switch' 
                            | 'case' | 'default' ;
    //  ----------------------------------------------------------------------------
        ConstDecl           ->  'const' Type ident Init ';' @OnConstDecl ;
        Init                ->  '=' ('false' | 'true' | ('+' | '-')? number) @OnInit ;
        VarDef              ->  Type  '*'? ident Init?
                            (',' '*'? ident Init? )* ';' @OnVarDef ;
        FuncDecl            ->  FuncHead ';' @OnFuncDecl ;
        FuncDefHead         ->  FuncHead @OnFuncDefHead;
        FuncDef             ->  FuncDefHead Block<Stat<NoBreakAdditionals>> 
                                @OnFuncDef ;
        FuncHeadReturn      ->  Type '*'? ident @OnFuncHeadReturnType ;
        FuncHead            ->  FuncHeadReturn '(' FormParList? ')' ;
        FormParList         ->  ( 'void' 
                                | Type '*'? ident ('[' ']')?
                                (',' Type '*'? ident ('[' ']')?)* ) @OnFormParList;
        Type                ->  'void' @OnTypeVoid 
                                | 'bool' @OnTypeBool 
                                | 'int' @OnTypeInt ;
        Block<S>            ->  '{' (ConstDecl | VarDef | S)* '}' @OnBlock ;
    //  ----------------------------------------------------------------------------
        BaseStat<TAdds>     ->  ( IncStat 
                                | DecStat 
                                | AssignStat 
                                | CallStat
                                | InputStat 
                                | OutputStat
                                | DeleteStat 
                                | ReturnStat 
                                | SwitchStat 
                                | ';'
                                | TAdds ) @OnBaseStat ;         
        NoBreakAdditionals  ->  Block<Stat<NoBreakAdditionals>> @OnStatBlock ; 
        BreakAdditionals    ->  BreakStat @OnStatBreakStat
                                | Block<Stat<BreakAdditionals>> @OnStatBlock ;
        Stat<TAdds>         ->  (IfStatIncomplete<TAdds>
                                | WhileStat
                                | BaseStat<TAdds>) @OnStat ;
        ThenStat<TAdds>     ->  (IfStatComplete<TAdds>
                                | WhileStatThen
                                | BaseStat<TAdds>) @OnStat ;
        IncStat             ->  ident '++' @OnIncStat ';' ;
        DecStat             ->  ident '--' @OnDecStat ';' ;
        AssignStat          ->  ident ('[' Expr ']' )? 
                                '=' Expr ';' @OnAssignStat ;
        CallStat            ->  ident '(' (ActParList)? ')' @OnCallStat ';' ;
        ActParList          ->  Expr ( ',' Expr )* @OnActParList ;
        IfStatComplete<TAdds>   -> 'if' '(' Expr ')' ThenStat<TAdds> 
                                    'else' ThenStat<TAdds> @OnIfStatElse ;

        IfStatIncomplete<TAdds> ->  ('if' '(' Expr ')' Stat<TAdds> @OnIfStat)
                                    | ('if' '(' Expr ')' ThenStat<TAdds> 
                                        'else' Stat<TAdds> @OnIfStatElse ) ;
        WhileBegin          ->  'while' @OnWhileStatBegin ;
        WhileStat           ->  WhileBegin '(' Expr ')' Stat<BreakAdditionals> 
                                @OnWhileStat ;
        WhileStatThen       ->  WhileBegin '(' Expr ')' ThenStat<BreakAdditionals> 
                                @OnWhileStat ;
        BreakStat           ->  'break' ';' @OnBreakStat ;
        InputStat           ->  'cin' '>>' ident ';' @InputStat ;
        SwitchStat          ->  'switch' '(' Expr ')' 
                                '{' CaseStat* (DefaultStat)? '}' 
                                @OnSwitchStat ;
        CaseStat            ->  'case' number ':' (Stat<BreakAdditionals>)* 
                                @OnCaseStat ;
        DefaultStat         ->  'default' ':' (Stat<BreakAdditionals>)* 
                                @OnDefaultStat ;
        OutputStat          ->  'cout' '<<' 
                                (Expr | string | 'endl') 
                                ('<<' ( Expr | string | 'endl'))* 
                                @OnOutputStatResult ';' ;
        DeleteStat          ->  'delete' '[' ']' ident ';' @OnDeleteStat ;
        ReturnStat          ->  'return' Expr? ';' @ReturnStat ;
    //  ----------------------------------------------------------------------------
        Expr                ->  OrExpr @OnExprResult ;
        OrExpr              ->  AndExpr ('||' AndExpr)* @OnOrExprResult ;
        AndExpr             ->  RelExpr ('&&' RelExpr)* @OnAndExprResult ;
        RelExpr             ->  SimpleExpr
                                (('==' | '!=' | '<' | '<=' | '>' | '>=') SimpleExpr)? 
                                @OnRelExprResult ;
        SimpleExpr          ->  ('+' | '-')?
                                Term 
                                (('+' | '-') 
                                    Term)* @OnSimpleExprResult ;
        Term                ->  NotFact (
                                ('*' | '/' | '%') 
                                NotFact)* @OnTermResult;
        NotFact             ->  '!'? Fact @OnNotFact ;
        Fact                ->  'false' @OnFactFalse 
                                | 'true' @OnFactTrue
                                | number @OnFactNumber
                                | ident
                                    (('[' Expr ']') 
                                        | ('(' (ActParList)?')')
                                    )? @OnFactIdent
                                | 'new' Type '[' Expr @OnFactNewArray ']'
                                |  '(' Expr @OnFactParanthesisExpr ')' ;
    }
}